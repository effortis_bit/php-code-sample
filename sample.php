<?php

require_once 'vendor/autoload.php';

use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use FacebookAds\Api;
use FacebookAds\Object\Ad;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\AdUser;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\AdCreative;
use FacebookAds\Object\Fields\AdCreativeFields;
use FacebookAds\Object\Fields\AdPreviewFields;
use FacebookAds\Object\Values\AdFormats;
use FacebookAds\Object\ObjectStorySpec;
use FacebookAds\Object\Fields\ObjectStorySpecFields;
use FacebookAds\Object\TargetingSearch;
use FacebookAds\Object\Search\TargetingSearchTypes;
use FacebookAds\Object\TargetingSpecs;
use FacebookAds\Object\Fields\TargetingSpecsFields;
use FacebookAds\Object\AdImage;
use FacebookAds\Object\Fields\AdImageFields;
use FacebookAds\Object\ObjectStory\LinkData;
use FacebookAds\Object\Fields\ObjectStory\LinkDataFields;
use FacebookAds\Object\Values\CallToActionTypes;
use FacebookAds\Object\ObjectStory\VideoData;
use FacebookAds\Object\Fields\ObjectStory\VideoDataFields;
use FacebookAds\Object\AdVideo;
use FacebookAds\Object\Fields\AdVideoFields;
use FacebookAds\Object\CustomConversion;
use FacebookAds\Object\Fields\CustomConversionFields;
use FacebookAds\Object\CustomAudience;
use FacebookAds\Object\Fields\CustomAudienceFields;
use FacebookAds\Object\Values\CustomAudienceSubtypes;
use FacebookAds\Object\Values\CustomAudienceTypes;





class Facebookapi extends CI_Controller
{


    public function getTable()
    {
        $this->Permission_model->check('facebook_api',FALSE);
        
        if(empty($dates = $this->input->post('dates')))
            $this->sendResponse();
        
        if(empty($facebook_ids = $this->input->post('facebook_ids')))
            $this->sendResponse(); 


        foreach ($facebook_ids as $facebook_id) {
            $account_ids[] = $facebook_id['account_id'];
            //$type_ids[] = $facebook_id['type_id'];
            $ids[$facebook_id['account_id']][] = $facebook_id['id'];
        }

        $this->load->model('Application_model');
        $this->load->model('Facebook_model');        
        
        $range = array('since' => $dates['from'],'until' => $dates['to']);
        $type_id = $this->input->post('type_id');        
        $class = 'FacebookAds\\Object\\'.Facebook_model::$types[$type_id];        
        
        $params['status'] = Application_model::getStatusId('Active');
        $params['ids'] = $account_ids;    
               
        $accountsDb = $this->Application_model->get($params);         
        
        $fields = array('spend','clicks','ctr','cpc','impressions','social_impressions','cpm','account_name','ad_name','adset_name','campaign_name','website_ctr','website_clicks');
        
        $breakdowns = $this->input->post('breakdowns');

        if(empty($breakdowns))
            $breakdowns = array();
        
        
        $apiparams['time_range'] = $range;
        
        if(!empty($breakdowns))
            $apiparams['breakdowns'] = $breakdowns;
        //$breakdowns = array();
        $dataFields = array_merge($fields,$breakdowns);


        if(!empty($accountsDb))
        {
            foreach ($accountsDb as $account) {
                Api::init(
                    $account['app_id'], 
                    $account['app_secret'],
                    $account['token'] 
                ); 
                foreach ($ids[$account['id']] as $facebookId) {
                    $total = array();
                    $facebookObj = new $class($facebookId);
                    
                    $creativeThumb = array();
                    if($type_id == Facebook_model::getTypeId('Ad'))
                    {
                        $thumbs = $this->Facebook_model->Get(array('api_id' => $facebookId,'type_id' => Facebook_model::getTypeId('Ad')));
                        $creativeThumb = $thumbs[0]->thumb;
                    }
                    
                    $insights = $facebookObj->getInsights($fields,$apiparams)->getArrayCopy();  

                    
                    $i=0;
                    if(!empty($insights))
                    foreach ($insights as $insight)
                    {                       
                        foreach ($dataFields as $field) {
                            if(!empty($insight->$field))
                            {                                
                                if($field == 'website_ctr')
                                {
                                    $data[$facebookId][$i][$field] = 0;
                                    if(!empty($insight->$field))
                                    {
                                        foreach ($insight->$field as $valueWebsite_ctr) {
                                            if($valueWebsite_ctr['action_type'] == 'link_click')
                                            {
                                                $data[$facebookId][$i][$field] = $valueWebsite_ctr['value'];
                                                break;
                                            }
                                        }
                                    }                                    
                                }
                                else
                                    $data[$facebookId][$i][$field] = $insight->$field;
                                
                                
                                if(in_array($field, array('account_name','ad_name','adset_name','campaign_name')))
                                    $total[$field] = $insight->$field;
                                else if(in_array($field, array('spend','clicks','impressions','social_impressions','cpm','cpc')))
                                    $total[$field] += $insight->$field;   
                                else if(in_array($field, array('age','gender','placement','impression_device')))
                                    $total[$field] = "All";
                                else if($field != 'website_ctr')
                                    $total[$field] += $insight->$field;
                            }
                            else
                                $data[$facebookId][$i][$field] = 0;
                        }    
                        $addText = '';
                        
                        $data[$facebookId][$i]['application_name'] = $account['name'];
                        

                        
                        if(!empty($breakdowns))
                        {
                            $addText = "&nbsp;&nbsp;&nbsp;&nbsp;";
                            foreach($breakdowns as $breakdown)
                            {
                                $addText .= ' '.$insight->$breakdown.'';
                            }
                            $data[$facebookId][$i]['application_name'] = $addText;
                            $data[$facebookId][$i]['campaign_name'] = $addText;
                            $data[$facebookId][$i]['adset_name'] = $addText;
                            $data[$facebookId][$i]['ad_name'] = $addText;                            
                        }    
                                               
                        
                        
                        if($creativeThumb && empty($breakdowns))
                            $data[$facebookId][$i]['thumbnail_url'] = $creativeThumb;
                        
                        $i++;
                    }     
                    

                    if(!empty($total['impressions'])) 
                    {
                        $total['ctr'] = $total['clicks'] / $total['impressions'] * 100;
                        $total['website_ctr'] = $total['website_clicks'] / $total['impressions'] * 100;
                        if($total['clicks'] != 0)
                        {
                            $total['cpc'] = $total['spend'] / $total['clicks'];
                        }
                        $total['cpm'] = $total['spend'] / $total['impressions'] * 1000;
                    }
                    
                    $total['application_name'] = $account['name'];
                    $total['thumbnail_url'] = $creativeThumb;
                    if(!empty($breakdowns) && !empty($insights))
                        $data[$facebookId][] = $total;   
                    
                    if($data[$facebookId])
                        $data[$facebookId] = array_reverse($data[$facebookId]);
                    
                }              
            }
        }
        $this->sendResponse($data);

    }
    
    public function getChart()
    {
        $this->Permission_model->check('facebook_api',FALSE);
        
        if(empty($dates = $this->input->post('dates')))
            $this->sendResponse();
        
        if(empty($facebook_ids = $this->input->post('facebook_ids')))
            $this->sendResponse();        

        foreach ($facebook_ids as $facebook_id) {
            $account_ids[] = $facebook_id['account_id'];
            $type_ids[] = $facebook_id['type_id'];
            $ids[$facebook_id['account_id']][$facebook_id['type_id']][] = $facebook_id['id'];
        }        
        
        $days = $this->getDays($dates['from'], $dates['to']);
        foreach ($days as $day) {
            $ranges[] = array('since' => $day,'until' => $day);
        }              

        $this->load->model('Application_model');
        $this->load->model('Facebook_model');
        
        $params['status'] = Application_model::getStatusId('Active');
        $params['ids'] = $account_ids;    
        $type_ids = array_unique($type_ids);       
        $accountsDb = $this->Application_model->get($params);        

        if(!empty($accountsDb))
        {
            foreach ($accountsDb as $account) {
                Api::init(
                    $account['app_id'], 
                    $account['app_secret'],
                    $account['token'] 
                );
                
                foreach ($type_ids as $type_id) {
                    if(!empty($ids[$account['id']][$type_id]))
                        foreach ($ids[$account['id']][$type_id] as $facebookId) {
                            $class = 'FacebookAds\\Object\\'.Facebook_model::$types[$type_id];
                            $facebookObj = new $class($facebookId);

                            $insights = $facebookObj->getInsights(array('spend','clicks','ctr','cpc','impressions','social_impressions','cpm','website_ctr','website_clicks'),array('time_ranges' => $ranges))->getArrayCopy();

                            
                            if(!empty($insights))
                            foreach ($insights as $insight) {
                                $data['insights'][$type_id][$facebookId][] = $insight;
                                $data['spend']['data'][$insight->date_start] += $insight->spend;
                                $data['clicks']['data'][$insight->date_start] += $insight->clicks;   
                                $data['website_clicks']['data'][$insight->date_start] += $insight->website_clicks;
                                $data['cpc']['data'][$insight->date_start] += $insight->cpc;
                                $data['cpc']['count'][$insight->date_start]++;
                                $data['cpm']['data'][$insight->date_start] += $insight->cpm;
                                $data['impressions']['data'][$insight->date_start] += $insight->impressions;
                                $data['social_impressions']['data'][$insight->date_start] += $insight->social_impressions;
                                $data['total']['spent'] += $insight->spend;
                                $data['total']['cpc'] += $insight->cpc;
                                $data['total']['clicks'] += $insight->clicks;
                                $data['total']['website_clicks'] += $insight->website_clicks;
                                $data['total']['impressions'] += $insight->impressions;
                                $data['spend']['info'] = "$";
                                $data['clicks']['info'] = "Clicks";
                                $data['ctr']['info'] = "%";
                                $data['website_ctr']['info'] = "%";
                                $data['cpc']['info'] = "$";
                                $data['cpm']['info'] = "$"; 
                                $data['impressions']['info'] = "";
                                $data['social_impressions']['info'] = "";
                                $data['spend']['label'] = "SPENT";
                                $data['clicks']['label'] = "CLICK";
                                $data['ctr']['label'] = "CTR";
                                $data['website_ctr']['label'] = "Website CTR";
                                $data['cpc']['label'] = "CPC";
                                $data['cpm']['label'] = "CPM"; 
                                $data['impressions']['label'] = "IMPR.";
                                $data['social_impressions']['label'] = "SOC IMPR."; 
                            }   

                        }                    
                }
            }
            if(!empty($data['impressions']['data']))
            {
                foreach ($data['impressions']['data'] as $date => $value) {
                    if(empty($value))
                        continue;
                    $ctr = $data['clicks']['data'][$date] / $value * 100;
                    $data['ctr']['data'][$date] = $ctr;
                    
                    $website_ctr = $data['website_clicks']['data'][$date] / $value * 100;
                    $data['website_ctr']['data'][$date] = $website_ctr;                    
                    
                    $cpc = 0;
                    if($data['clicks']['data'][$date] != 0)
                        $cpc = $data['spend']['data'][$date] / $data['clicks']['data'][$date];
                    
                    $data['cpc']['data'][$date] = $cpc; 
                    
                    $cpm = $data['spend']['data'][$date] / $value * 1000;
                    $data['cpm']['data'][$date] = $cpm;                    
                }
                $ctr_total = $data['total']['clicks'] / $data['total']['impressions'] * 100;
                $data['total']['ctr'] = $ctr_total;

                $website_ctr_total = $data['total']['website_clicks'] / $data['total']['impressions'] * 100;
                $data['total']['website_ctr'] = $website_ctr_total;
                
                $cpm_total = $data['total']['spent'] / $data['total']['impressions'] * 1000;
                $data['total']['cpm'] = $cpm_total;                
                
                if($data['total']['clicks'] != 0)
                    $cpc_total = $data['total']['spent'] / $data['total']['clicks'];
                else
                    $cpc_total = 0;
                $data['total']['cpc'] = $cpc_total;                
            }
            
        } 
        if(!empty($data))
        {
            $data['total']['days'] = $days;
            foreach ($data as $key => $value) {
                if(!empty($data[$key]['data']))
                {
                    ksort($data[$key]['data']);
                }
            }
        }
        
        unset($data['website_clicks']);
        
        $this->sendResponse($data);
    }
	
}
	
	
	
	
	
	